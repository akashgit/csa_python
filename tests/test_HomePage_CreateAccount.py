import time

import pytest

from pageObjects.HomePage import Home_page
from pageObjects.Register import Register_Login
from pageObjects.RegisterForm import Register_Form
from testData.SearchData import SearchPageData

from utilities.BaseClass import BaseClass


class TestHome_Register(BaseClass):

    def test_e2e_Register(self, getData):
        log = self.getLogger()
        log.info("Browser & URL Invoked")

        hp = Home_page(self.driver)
        hp.Click_Login_Register().click()
        log.info("Login/Register Button clicked")
        time.sleep(5)

        reg = Register_Login(self.driver)
        reg.Create_Account().click()
        log.info("Register Button clicked")
        time.sleep(5)

        rf = Register_Form(self.driver)
        rf.First_Name().send_keys(getData["fname"])
        log.info("First Name Entered")
        rf.Last_Name().send_keys(getData["lname"])
        log.info("Last Name Entered")
        time.sleep(3)
        self.selectOptionByText(rf.Country(), getData["country_select"])
        time.sleep(3)
        log.info("Country Selected")
        self.selectOptionByText(rf.Industry(), getData["Industry_Select"])
        log.info("Industry selected")
        rf.Job_Title().send_keys(getData["Job_Title"])
        log.info("Job title entered")

        rf.Email().send_keys(getData["Email"])
        log.info("Email entered")
        rf.Password().send_keys(getData["Password"])
        log.info("Password entered")
        rf.Password_Confirm().send_keys(getData["Password_Conf"])
        log.info("Confirm password entered")

        self.selectTerms(rf.Tarms(),getData["Checkbox_Value"])
        log.info("Terms clicked")


        # rf.CreateAccount().click()
        # log.info("Account created")

    @pytest.fixture(params=SearchPageData.getTestData("Register_1"))
    def getData(self, request):
        return request.param
