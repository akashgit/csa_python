import time

import pytest

from pageObjects.HomePage import Home_page
from pageObjects.Register import Register_Login
from testData.SearchData import SearchPageData
from utilities.BaseClass import BaseClass


class Test_Login_Logout(BaseClass):

    def test_e2e_LoginLogout(self, getData):
        log = self.getLogger()
        log.info("Browser & URL Invoked")

        hp = Home_page(self.driver)
        hp.Click_Login_Register().click()
        log.info("Login/Register Button clicked")
        time.sleep(5)

        reg = Register_Login(self.driver)
        reg.User_Name().send_keys(getData["Email"])
        log.info("Valid user name entered")
        reg.User_Password().send_keys(getData["Password"])
        log.info("Valid password entered")
        reg.User_Login().click()
        log.info("Login button clicked")
        time.sleep(5)

        hp.LogoutButton().click()
        log.info("Logout button clicked")

    @pytest.fixture(params=SearchPageData.getTestData("Register_1"))
    def getData(self, request):
        return request.param