import time

import pytest

from pageObjects.HomePage import Home_page
from pageObjects.MyAccount import My_Account
from pageObjects.Register import Register_Login
from testData.SearchData import SearchPageData
from utilities.BaseClass import BaseClass


class Test_My_Account(BaseClass):

    def test_e2e_LoginLogout(self, getData):
        log = self.getLogger()
        log.info("Browser & URL Invoked")

        hp = Home_page(self.driver)
        hp.Click_Login_Register().click()
        log.info("Login/Register Button clicked")
        time.sleep(5)

        reg = Register_Login(self.driver)
        reg.User_Name().send_keys(getData["Email"])
        log.info("Valid user name entered")
        reg.User_Password().send_keys(getData["Password"])
        log.info("Valid password entered")
        reg.User_Login().click()
        log.info("Login button clicked")
        time.sleep(3)

        ######################################################################

        hp.MyAccountButton().click()
        log.info("My Account Button clicked")

        mac = My_Account(self.driver)

        mac.ContactInformatin().click()
        log.info("Contact Information clicked")
        Expected_compny = mac.Company_Name().text()
        Actual_company = getData["Company_Name"]
        assert Expected_compny == Actual_company
        log.info("Expected_compny matched with Actual_company")

        mac.ChangePassword().click()
        time.sleep(3)
        Expected_Change_Pass_text = mac.CurrentPass_Text().text()
        Actual_Change_Pass_text = getData["Change_Pass_1"]
        assert Expected_Change_Pass_text == Actual_Change_Pass_text
        log.info("Current Password field appear")

        Expected_New_Password_text = mac.NewPassword_Text().text()
        Actual_New_Password_text = getData["Change_Pass_2"]
        assert Expected_New_Password_text == Actual_New_Password_text
        log.info("New Password field appear")

        Expected_Confirm_Password_text = mac.ConfirmPassword_Text().text()
        Actual_Confirm_Password_text = getData["Change_Pass_3"]
        assert Expected_Confirm_Password_text == Actual_Confirm_Password_text
        log.info("Confirm New Password field appear")

        ############################################################################

    @pytest.fixture(params=SearchPageData.getTestData("Register_1"))
    def getData(self, request):
        return request.param
