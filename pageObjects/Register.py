from selenium.webdriver.common.by import By

from pageObjects.RegisterForm import Register_Form


class Register_Login:

    def __init__(self, driver):
        self.driver = driver

    user = (By.XPATH, "//input[@id='emailField']")

    def User_Name(self):
        return self.driver.find_element(*Register_Login.user)

    pwd = (By.XPATH, "//input[@id='passwordField']")

    def User_Password(self):
        return self.driver.find_element(*Register_Login.pwd)

    logn = (By.XPATH, "//input[@id='send2Dsk']")

    def User_Login(self):
        return self.driver.find_element(*Register_Login.logn)

    create = (By.XPATH, "//input[contains(@class,'btn btn-default btn-sm register cc_register')]")

    def Create_Account(self):
        return self.driver.find_element(*Register_Login.create)
