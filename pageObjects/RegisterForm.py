from selenium.webdriver.common.by import By


class Register_Form:

    def __init__(self, driver):
        self.driver = driver

    f_name = (By.XPATH, "//input[@id='firstName']")

    def First_Name(self):
        return self.driver.find_element(*Register_Form.f_name)

    l_name = (By.XPATH, "//input[@id='lastName']")

    def Last_Name(self):
        return self.driver.find_element(*Register_Form.l_name)

    ctry = (By.XPATH, "//select[@name='billingAddress.countryCode']")

    def Country(self):
        return self.driver.find_element(*Register_Form.ctry)

    indus = (By.XPATH, "//select[@name='industry']")

    def Industry(self):
        return self.driver.find_element(*Register_Form.indus)

    jobtit = (By.ID, "jobTitle")

    def Job_Title(self):
        return self.driver.find_element(*Register_Form.jobtit)

    ######################################################################################

    eml = (By.CSS_SELECTOR, "#username")

    def Email(self):
        return self.driver.find_element(*Register_Form.eml)

    pwd = (By.XPATH, "//input[@id='NewPassword']")

    def Password(self):
        return self.driver.find_element(*Register_Form.pwd)

    new_pwd = (By.XPATH, "//input[@id='ConfirmPassword']")

    def Password_Confirm(self):
        return self.driver.find_element(*Register_Form.new_pwd)

    #########################################################################################

    terms = (By.XPATH, "//input[@type='checkbox']")

    def Tarms(self):
        return self.driver.find_elements(*Register_Form.terms)



    #########################################################################################

    create = (By.XPATH, "//input[@id='btn-registration-submit']")

    def CreateAccount(self):
        return self.driver.find_element(*Register_Form.create)