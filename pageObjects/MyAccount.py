from selenium.webdriver.common.by import By


class My_Account:

    def __init__(self, driver):
        self.driver = driver

    ###########################################################

    continfo = (By.XPATH, "//a[@id='acct-href0']")

    def ContactInformatin(self):
        return self.driver.find_element(*My_Account.continfo)

    comp = (By.XPATH, "//p[@class='myAccProfileCompany cc_profile_company']/span[2]")

    def Company_Name(self):
        return self.driver.find_element(*My_Account.comp)

    ##############################################################

    changepass = (By.XPATH, "//a[@id='acct-href1']")

    def ChangePassword(self):
        return self.driver.find_element(*My_Account.changepass)

    curpass_text = (By.XPATH, "//span[contains(text(),'Current Password')]")

    def CurrentPass_Text(self):
        return self.driver.find_element(*My_Account.curpass_text)

    npass_text = (By.XPATH, "//label[@class='col-xs-3 cc_new_password_label']/span")

    def NewPassword_Text(self):
        return self.driver.find_element(*My_Account.npass_text)

    confnpass = (By.XPATH, "//label[@class='col-xs-3 cc_newpasswordconfirm_label']/span")

    def ConfirmPassword_Text(self):
        return self.driver.find_element(*My_Account.confnpass)
